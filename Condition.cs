
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CloudTables
{
	public class Condition
	{
		private string _id = null;
		private string _name = null;
		private string _op = "=";
		private bool _set = true;
		private string _setValue = null;
		private string _value = null;

        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Constructors
         */

        /// <summary>
        /// Create an empty condition instance
        /// </summary>
        public Condition()
        {
        }

        /// <summary>
        /// Create condition instance with an id and value
        /// </summary>
        /// <param name="id">Condition data point</param>
        /// <param name="value">Condition value</param>
        public Condition(string id, string value)
        {
            _id = id;
            _value = value;
        }

        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Methods
         */

        /// <summary>
        /// Set the id of the data point to condition on (use this OR name, not both)
        /// </summary>
        /// <param name="id">Id</param>
		public Condition Id(string id)
		{
			_id = id;

			return this;
		}

        /// <summary>
        /// Set the name of the data point to condition on (use this OR id, not both)
        /// </summary>
        /// <param name="name">Name</param>
		public Condition Name(string name)
		{
			_name = name;

			return this;
		}

        /// <summary>
        /// Set condition operator: '=' | '!=' | '<' | '<=' | '>=' | '>'
        /// </summary>
        /// <param name="op">Operator</param>
		public Condition Op(string op)
		{
			_op = op;

			return this;
		}

        /// <summary>
        /// Turn set value on or off
        /// </summary>
        /// <param name="set">Set value when creating a new row</param>
		public Condition Set(bool set)
		{
			_set = set;

			return this;
		}

        /// <summary>
        /// Value that will be written to the database when creating a new
		/// row (overrides `value` which is used if this is not defined).
        /// </summary>
        /// <param name="setValue">setValue</param>
		public Condition SetValue(string setValue)
		{
			_setValue = setValue;

			return this;
		}

        /// <summary>
        /// Set the value to conidtion on. For numbers, format as a string.
        /// </summary>
        /// <param name="value">Value</param>
		public Condition Value(string value)
		{
			_value = value;

			return this;
		}



        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Internal
         */
        /// <summary>
        /// Add to dictionary for HTTP request
        /// </summary>
        /// <param name="dic">Dictionary</param>
        /// <param name="pos">Position in array</param>
		internal void DictionaryEntry(Dictionary<string, string> dic, int pos)
		{
			if (_id != null) {
				dic.Add("conditions[" + pos + "][id]", _id);
			}

			if (_name != null) {
				dic.Add("conditions[" + pos + "][name]", _name);
			}

			if (_op != null) {
				dic.Add("conditions[" + pos + "][op]", _op);
			}

			dic.Add("conditions[" + pos + "][set]", _set ? "true" : "false");

			if (_setValue != null) {
				dic.Add("conditions[" + pos + "][setValue]", _setValue);
			}

			if (_value != null) {
				dic.Add("conditions[" + pos + "][value]", _value);
			}
		}
	}
}
