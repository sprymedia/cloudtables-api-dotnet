using System;
using System.Net;

namespace CloudTables
{
	public class HttpException: Exception {
		public HttpStatusCode statusCode { get; set; }

		public HttpException(string message, HttpStatusCode code): base(message) {
			statusCode = code;
		}
	}

	public class InsecureException: Exception {
		public InsecureException(string message): base(message) {}
	}
}
