
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CloudTables
{
	/// <summary>
	/// Manipulate an existing row, including getting its data.
	/// </summary>
	public class Row
	{
		private Dataset _ds;
		private int _id;

        /// <summary>
        /// Create a row instance
        /// </summary>
        /// <param name="host">Dataset host</param>
        /// <param name="id">Data set ID</param>
		public Row(Dataset host, int id)
		{
			_ds = host;
			_id = id;
		}

		/// <summary>
		/// Get the data for a specific row
		/// </summary>
		/// <returns>Data for the data set</returns>
		public async Task<TRowData>Data()
		{
			var json = await _ds.Api().Request<TRowData>(
				"/api/1/dataset/" + _ds.Id() + "/" + _id,
				"GET"
			);

			return json;
		}

		/// <summary>
		/// Delete a row
		/// </summary>
		/// <returns>Success flag / error message if an error occurs</returns>
		public async Task<TDelete> Delete()
		{
			var json = await _ds.Api().Request<TDelete>(
				"/api/1/dataset/" + _ds.Id() + "/" + _id,
				"DELETE"
			);

			return json;
		}

		/// <summary>
		/// Get the ID this row instance operates on
		/// </summary>
		/// <returns>Row id</returns>
		public int Id()
		{
			return _id;
		}

		/// <summary>
		/// Insert a new row into the data set
		/// </summary>
		/// <param name="data">Data to insert as a new row</param>
		/// <returns>Newly inserted row's data</returns>
		public async Task<TRowEditData>Update(IEnumerable<KeyValuePair<string, string>> data)
		{
			var json = await _ds.Api().Request<TRowEditData>(
				"/api/1/dataset/" + _ds.Id() + "/" + _id,
				"PUT",
				data
			);

			return json;
		}
	}
}