
namespace CloudTables
{
	public class TAccess
	{
		public string error;
		public string token;
	}

	public class TDatasetColumn
	{
		public string id;
		public string link;
		public string name;
	}

	public class TDatasetData
	{
		public TDatasetColumn[] columns;
		public dynamic[] data;
		public string error;
		public bool success;
	}

	public class TDatasetInfo
	{
		public int deleteCount;
		public string id;
		public int insertCount;
		public string lastData;
		public string name;
		public int readCount;
		public int rowCount;
		public int updateCount;
	}

	public class TDatasets
	{
		public TDatasetInfo[] datasets;
	}

	public class TError
	{
		public string error;
		public string message;
	}

	public class TDataset {
		public dynamic[] data;
		public string error;
		public bool success;
	}

	public class TComputed {
		public string id;
		public string name;
	}

	public class TDatapoints {
		public string id;
		public string name;
	}

	public class TLinkTarget {
		public string id;
	}

	public class TLinks: TPoints {
		public string id;
		public string name;
		public TLinkTarget target;
	}

	public class TPoints {
		public TComputed[] computed;
		public TDatapoints[] datapoints;
		public TLinks[] links;
	}

	public class TApiColumn {
		public string data;
		public string id;
		public string link;
		public string name;
	}

	public class TSchema: TPoints {
		public string description;
		public string name;
		public bool success;
		public TApiColumn[] table;
	}

	public class TDelete {
		public string error;
		public bool success;
	}

	public class TRowData {
		public dynamic data;
		public string error;
		public bool success;
	}

	public class TRowEditData {
		public dynamic data;
		public string error;
		public TFieldErrors[] fieldErrors;
		public bool success;
	}

	public class TFieldErrors {
		public string name;
		public string status;
	}
}
