﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CloudTables;

namespace Cli
{
    class Program
    {
        private static string _apiKey;

        private static string _clientId;

        private static string _clientName;

        private static string _command;

        private static string _conditionId = null;

        private static string _conditionName = null;

        private static string _conditionOp = null;

        private static string _conditionSet = null;

        private static string _conditionValue = null;

        private static string _dataset;

        private static string _domain = null;

        private static Dictionary<string, string> _dict;

        private static Int32 _row;

        private static string _ssl = null;

        private static string _secure = null;

        private static string _subdomain = null;

        // private static string _values = null;
        private static void usage()
        {
            Console
                .WriteLine("Usage: cli [options] --apiKey key --command cmd --subdomain id");
            Console.WriteLine("Options: ");
            Console.WriteLine("\t--dataset datasetId");
            Console.WriteLine("\t--clientId id");
            Console.WriteLine("\t--clientName name");
            Console.WriteLine("\t--conditionId id");
            Console.WriteLine("\t--conditionName name");
            Console.WriteLine("\t--conditionOp op");
            Console.WriteLine("\t--conditionSet true|false");
            Console.WriteLine("\t--conditionValue value");
            Console.WriteLine("\t--domain value");
            Console.WriteLine("\t--row id");
            Console.WriteLine("\t--secure true|false");
            Console.WriteLine("\t--ssl true|false");
            Console.WriteLine("\t--values valuesToSet 'dp-1=2;dp-2=fred;'");
            Console.WriteLine("Commands are:");
            Console
                .WriteLine("\tdatasetData datasetSchema datasets insert rowDelete rowUpdate scriptTag token");

            System.Environment.Exit(1);
        }

        // make sure a value has been provided
        private static void checkParams(string param)
        {
            if (param == null)
            {
                usage();
            }
        }

        // Get the optArg for the switch
        private static string getOptArg(string[] args, int pos)
        {
            if (args.Length == pos + 1)
            {
                usage();
            }
            if (args[pos + 1].StartsWith("--"))
            {
                usage();
            }

            return args[pos + 1];
        }

        // Get the list of arg
        private static void getArgs(string[] args)
        {
            for (int i = 0; i < args.Length; i++)
            {
                switch (args[i])
                {
                    case "--apiKey":
                        _apiKey = getOptArg(args, i++);
                        break;
                    case "--clientId":
                        _clientId = getOptArg(args, i++);
                        break;
                    case "--clientName":
                        _clientName = getOptArg(args, i++);
                        break;
                    case "--command":
                        _command = getOptArg(args, i++);
                        break;
                    case "--conditionId":
                        _conditionId = getOptArg(args, i++);
                        break;
                    case "--conditionName":
                        _conditionName = getOptArg(args, i++);
                        break;
                    case "--conditionOp":
                        _conditionOp = getOptArg(args, i++);
                        break;
                    case "--conditionSet":
                        _conditionSet = getOptArg(args, i++);
                        break;
                    case "--conditionValue":
                        _conditionValue = getOptArg(args, i++);
                        break;
                    case "--dataset":
                        _dataset = getOptArg(args, i++);
                        break;
                    case "--domain":
                        _domain = getOptArg(args, i++);
                        break;
                    case "--row":
                        _row = Int32.Parse(getOptArg(args, i++));
                        break;
                    case "--secure":
                        _secure = getOptArg(args, i++);
                        break;
                    case "--subdomain":
                        _subdomain = getOptArg(args, i++);
                        break;
                    case "--ssl":
                        _ssl = getOptArg(args, i++);
                        break;
                    case "--values":
                        _dict =
                            getOptArg(args, i++)
                                .Split(';')
                                .Select(part => part.Split('='))
                                .Where(part => part.Length == 2)
                                .ToDictionary(sp => sp[0], sp => sp[1]);
                        break;
                    default:
                        usage();
                        break;
                }
            }

            // Verify mandatory params
            checkParams (_apiKey);
            checkParams (_subdomain);
            checkParams (_command);
        }

        private static Condition checkCondition(Condition condition)
        {
            if (condition == null)
            {
                condition = new Condition();
            }

            return condition;
        }

        // Get the list of arg
        private static Condition getCondition()
        {
            Condition condition = null;

            if (_conditionId != null)
            {
                condition = checkCondition(condition);
                condition.Id (_conditionId);
            }
            if (_conditionName != null)
            {
                condition = checkCondition(condition);
                condition.Name (_conditionName);
            }
            if (_conditionOp != null)
            {
                condition = checkCondition(condition);
                condition.Op (_conditionOp);
            }
            if (_conditionSet != null)
            {
                condition = checkCondition(condition);
                condition.Set(_conditionSet == "true");
            }
            if (_conditionValue != null)
            {
                condition = checkCondition(condition);
                condition.Value (_conditionValue);
            }

            return condition;
        }

        // Run the command!
        private static async Task runCommand(string[] arg)
        {
            var api =
                _subdomain == ""
                    ? new Api(_apiKey)
                    : new Api(_subdomain, _apiKey);
            var condition = getCondition();

            if (condition != null)
            {
                api.Condition (condition);
            }
            if (_clientId != null)
            {
                api.ClientId (_clientId);
            }
            if (_clientName != null)
            {
                api.ClientName (_clientName);
            }
            if (_domain != null)
            {
                api.Domain (_domain);
            }
            if (_secure != null)
            {
                api.Secure(_secure == "true");
            }
            if (_ssl != null)
            {
                api.Ssl(_ssl == "true");
            }

            switch (_command)
            {
                case "datasetData":
                    checkParams (_dataset);

                    try
                    {
                        Console
                            .WriteLine(Newtonsoft
                                .Json
                                .JsonConvert
                                .SerializeObject(await api
                                    .Dataset(_dataset)
                                    .Data()));
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    break;
                case "datasetSchema":
                    checkParams (_dataset);

                    try
                    {
                        Console
                            .WriteLine(Newtonsoft
                                .Json
                                .JsonConvert
                                .SerializeObject(await api
                                    .Dataset(_dataset)
                                    .Schema()));
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    break;
                case "datasets":
                    Console
                        .WriteLine(Newtonsoft
                            .Json
                            .JsonConvert
                            .SerializeObject(await api.Datasets()));
                    break;
                case "insert":
                    try
                    {
                        Console
                            .WriteLine(Newtonsoft
                                .Json
                                .JsonConvert
                                .SerializeObject(await api
                                    .Dataset(_dataset)
                                    .Insert(_dict)));
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    break;
                case "rowDelete":
                    try
                    {
                        Console
                            .WriteLine(Newtonsoft
                                .Json
                                .JsonConvert
                                .SerializeObject(await api
                                    .Dataset(_dataset)
                                    .Row(_row)
                                    .Delete()));
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    break;
                case "rowUpdate":
                    try
                    {
                        Console
                            .WriteLine(Newtonsoft
                                .Json
                                .JsonConvert
                                .SerializeObject(await api
                                    .Dataset(_dataset)
                                    .Row(_row)
                                    .Update(_dict)));
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    break;
                case "scriptTag":
                    Console
                        .WriteLine(await api
                            .Dataset(_dataset)
                            .ScriptTagAsync());
                    break;
                case "token":
                    Console.WriteLine(await api.Token());
                    break;
                default:
                    Console.WriteLine("Unknown command: " + _command);
                    usage();
                    break;
            }
        }

        static void Main(string[] args)
        {
            getArgs (args);
            runCommand(args).GetAwaiter().GetResult();
        }
    }
}
