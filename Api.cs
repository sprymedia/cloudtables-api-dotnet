﻿
using System;
using System.Collections.Generic;
using System.Net; // Used for the .NET 4.5 build
using System.Net.Http;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CloudTables
{
    public class Api
    {
        private string _accessToken = null;
        private string _clientId = null;
        private string _clientName = null;
        private string _domain = "cloudtables.io";
        private int _duration = -1;
        private string _key;
        private List<string> _roles = new List<string>();
        private bool _secure = true;
        private bool _ssl = true;
        private string _subDomain;
        private List<Condition> _conditions = new List<Condition>();


        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Constructors
         */

        /// <summary>
        /// Create the API instance
        /// </summary>
        /// <param name="subDomain">Application name (your CloudTables sub-domain)</param>
        /// <param name="key">API key</param>
        public Api(string subDomain, string key)
        {
            _subDomain = subDomain;
            _key = key;
        }

        /// <summary>
        /// Create the API instance for self hosted installs
        /// </summary>
        /// <param name="key">API key</param>
        public Api(string key)
        {
            _subDomain = "";
            _key = key;
        }

        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Public methods
         */

        /// <summary>
        /// Your unique identifier for the user
        /// </summary>
        /// <param name="set">Client ID</param>
        /// <returns>Self for chaining</returns>
        public Api ClientId(string set)
        {
            _clientId = set;

            return this;
        }

        /// <summary>
        /// Name / label to give the use in the CloudTables configuration UI
        /// </summary>
        /// <param name="set">Client name</param>
        /// <returns>Self for chaining</returns>
        public Api ClientName(string set)
        {
            _clientName = set;

            return this;
        }

        /// <summary>
        /// Conditions to apply to the data to fetch and write
        /// </summary>
        /// <param name="set">Condition instance</param>
        /// <returns>Self for chaining</returns>
        public Api Condition(Condition set)
        {
            _conditions.Add(set);

            return this;
        }

        /// <summary>
        /// Get the data and columns for a given dataset
        /// </summary>
        /// <param name="id">Dataset id</param>
        /// <returns>Data set columns and data</returns>
        [Obsolete("Please use Dataset().Data() instead")]
        public async Task<TDatasetData> Data(string id)
        {
            var res = await Request<TDatasetData>("/api/1/dataset/" + id + "/data", "GET");
 
            return res;
        }

        /// <summary>
        /// Manipulate a specific data set
        /// </summary>
        /// <param name="id">Data set id</param>
        /// <returns>Data set instance</returns>
        public Dataset Dataset(string id)
        {
            var ds = new Dataset(this, id);

            return ds;
        }

        /// <summary>
        /// Get summary information about the available datasets
        /// </summary>
        /// <returns>Information about the available datasets</returns>
        public async Task<TDatasetInfo[]> Datasets()
        {
            var res = await Request<TDatasets>("/api/1/datasets", "GET");

            return res.datasets;
        }

        /// <summary>
        /// Get host domain
        /// </summary>
        /// <returns>Host</returns>
        internal string Domain()
        {
            return _domain;
        }

        /// <summary>
        /// Host domain for the CloudTables instance
        /// </summary>
        /// <param name="set">TLD</param>
        /// <returns>Self for chaining</returns>
        public Api Domain(string set)
        {
            _domain = set;

            return this;
        }

        /// <summary>
        /// Token expire duration
        /// </summary>
        /// <param name="set">Duration</param>
        /// <returns>Self for chaining</returns>
        public Api Duration(int set)
        {
            _duration = set;

            return this;
        }

        /// <summary>
        /// Add a role to the access rights (union'ed with the roles for the API key)
        /// </summary>
        /// <param name="set">Role name</param>
        /// <returns>Self for chaining</returns>
        public Api Role(string set)
        {
            _roles.Add(set);

            return this;
        }

        /// <summary>
        /// Add multiple roles to the access rights (union'ed with the roles for the API key)
        /// </summary>
        /// <param name="set">Role name</param>
        /// <returns>Self for chaining</returns>
        public Api Roles(IEnumerable<string> set)
        {
            foreach(var entry in set) {
                _roles.Add(entry);
            }

            return this;
        }

        /// <summary>
        /// Get the `script` tag to use to embed a given dataset, including the access token that
        /// will grant the user access (i.e. no need to call `Token()` yourself).
        /// </summary>
        /// <param name="id">ID (UUID) of the dataset to embed</param>
        /// <param name="style">Styling framework to use</param>
        /// <returns>Script tag to embed the dataset into an HTML page</returns>
        [Obsolete("Please use Dataset().ScriptTagAsync() instead")]
        public async Task<string> ScriptTag(string id, string style = "d")
        {
            var ds = Dataset(id);
            var tag = await ds.ScriptTagAsync(style);

            return tag;
        }

        /// <summary>
        /// Allow self signed certificates (`false`) or not (`true` - default). Note that this
        /// is not supported with .NET Standard 1.x (2.0 or .NET Framework required)
        /// </summary>
        /// <param name="set">Indicator</param>
        /// <returns>Self for chaining</returns>
        public Api Secure(bool set)
        {
            _secure = set;

            return this;
        }

        /// <summary>
        /// Get SSL status.
        /// </summary>
        /// <returns>SSL flag</returns>
        internal bool Ssl()
        {
            return _ssl;
        }

        /// <summary>
        /// Use https (`true`) or http (`false`) connections (for self hosted installs).
        /// </summary>
        /// <param name="set">Indicator</param>
        /// <returns>Self for chaining</returns>
        public Api Ssl(bool set)
        {
            _ssl = set;

            return this;
        }

        /// <summary>
        /// Get Subdomain status.
        /// </summary>
        /// <returns>Subdomain</returns>
        internal string Subdomain()
        {
            return _subDomain;
        }

        /// <summary>
        /// Get an Access Token for CloudTables
        /// </summary>
        /// <param name="set">Indicator</param>
        /// <returns>Self for chaining</returns>
        public async Task<string> Token()
        {
            if (_accessToken != null) {
                return _accessToken;
            }

            var data = new Dictionary<string, string>();

            if (_clientId != null) {
                data.Add("clientId", _clientId);
            }

            if (_clientName != null) {
                data.Add("clientName", _clientName);
            }

            if (_duration != -1) {
                data.Add("duration", _duration.ToString());
            }

            var json = await Request<TAccess>("/api/1/access", "POST", data);

            // Save - in case used multiple times.
            _accessToken = json.token;

            return json.token;
        }

        internal async Task<T> Request<T>(
            string path,
            string type,
            IEnumerable<KeyValuePair<string, string>> dataIn = null,
            string separator = "_"
        ) {
            var httpClientHandler = new HttpClientHandler();
            HttpResponseMessage result;

            if (! this._secure) {
#if NETSTANDARD1_3
                throw new InsecureException("Insecure cannot be used with .NET Standard 1.x - use 2.0 or newer");
#elif NET45
                ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;   
#else
                // Allow self signed certificates
                httpClientHandler.ServerCertificateCustomValidationCallback =
                    (sender, certificate, chain, errors) => {
                        return true;
                    };
#endif
            }

            var client = new HttpClient(httpClientHandler);
            var protocol = _ssl
                ? "https://"
                : "http://";
            var host = _subDomain != ""
                ? protocol + _subDomain + "." + _domain
                : protocol + _domain + "/io"; // self hosted uses `/io` prefix on path

            if (dataIn == null) {
                dataIn = new Dictionary<string, string>();
            }

            // Required / global parameters
            var data = dataIn.ToDictionary(kvp=>kvp.Key, kvp=>kvp.Value);
            data.Add("key", _key);

            if (_roles.Count != 0) {
                data.Add("roles", _roles.ToString());
            }

            if (separator != null) {
                data.Add("separator", separator);
            }

            for (var i = 0; i < _conditions.Count; i++) {
                _conditions[i].DictionaryEntry(data, i);
            }
            

            if (type == "GET" || type == "DELETE") {
                // Add escaped query parameters
                var query = "";

                if (type == "GET" || type == "DELETE") {
                    foreach(var entry in data) {
                        query += entry.Key + "=" + Uri.EscapeUriString(entry.Value) + "&";
                    }
                }

                result = type == "GET"
                    ? await client.GetAsync(host + path + "?" + query)
                    : await client.DeleteAsync(host + path + "?" + query);
            }
            else if (type == "POST" || type == "PUT") {
                var content = new FormUrlEncodedContent(data);

                result = type == "POST"
                    ? await client.PostAsync(host + path, content)
                    : await client.PutAsync(host + path, content);
            }
            else {
                throw new Exception("Unknown request type");
            }

            var resultContent = await result.Content.ReadAsStringAsync();
            var statusCode = (int)result.StatusCode;

            client.Dispose();

            // Console.WriteLine("resultContent: " + resultContent);

            // Change from a JSON string into our actual data
            var json = JsonConvert.DeserializeObject<T>(resultContent);
            return json;
        }
    }
}
