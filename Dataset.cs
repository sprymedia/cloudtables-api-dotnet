
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CloudTables
{
	/// <summary>
	/// Manipulate and get information about a data set.
	/// </summary>
    public class Dataset
	{
		private Api _host;
		private string _id;

        /// <summary>
        /// Create a data set instance
        /// </summary>
        /// <param name="host">CloudTables API host</param>
        /// <param name="id">Data set ID</param>
		public Dataset(Api host, string id)
		{
			_host = host;
			_id = id;
		}

		/// <summary>
		/// Get the data for a given dataset
		/// </summary>
		/// <returns>Data for the data set</returns>
		public async Task<TDataset>Data()
		{
			var json = await _host.Request<TDataset>("/api/1/dataset/" + this._id, "GET");

			return json;
		}

		/// <summary>
		/// Get the data set ID that this instance operates on
		/// </summary>
		/// <returns>Data set id</returns>
		public string Id()
		{
			return _id;
		}

		/// <summary>
		/// Insert a new row into the data set
		/// </summary>
		/// <param name="data">Data to insert as a new row</param>
		/// <returns>Newly inserted row's data</returns>
		public async Task<TRowEditData>Insert(IEnumerable<KeyValuePair<string, string>> data)
		{
			var json = await _host.Request<TRowEditData>(
				"/api/1/dataset/" + _id,
				"POST",
				data
			);

			return json;
		}

		/// <summary>
		/// Get a Row instance to work on a specific row
		/// </summary>
		/// <param name="id">Row id</param>
		/// <returns>Row instance</returns>
		public Row Row(int id)
		{
			var row = new Row(this, id);

			return row;
		}

		/// <summary>
		/// Get the data structure for the data set
		/// </summary>
		/// <returns>Data set structure</returns>
		public async Task<TSchema>Schema()
		{
			var json = await _host.Request<TSchema>(
				"/api/1/dataset/" + _id + "/schema",
				"GET"
			);

			return json;
		}

		/// <summary>
		/// Get a `<script>` tag for a specific dataset to be displayed as a table. Insert this script tag
		/// where you want the table to appear in your HTML.
		/// </summary>
		/// <param name="token">Secure access token (from `.token()`)</param>
		/// <param name="style">The styling framework to use for this table</param>
		/// <returns>`<script>` tag to use.</returns>
		public string ScriptTag(string token, string style = "d")
		{
            var protocol = _host.Ssl()
                ? "https://"
                : "http://";

            var host = _host.Subdomain() != ""
                ? protocol + _host.Subdomain() + "." + _host.Domain()
                : protocol + _host.Domain() + "/io"; // self hosted uses `/io` prefix on path

            return "<script src=\"" + host + "/loader/" + _id +
                "/table/" + style + "\" data-token=\"" + token + "\"></script>";
		}

		/// <summary>
		/// Get a `<script>` tag for a specific dataset to be displayed as a table. Insert this script tag
		/// where you want the table to appear in your HTML.
		/// </summary>
		/// <param name="token">Secure access token (from `.token()`)</param>
		/// <param name="style">The styling framework to use for this table</param>
		/// <returns>`<script>` tag to use.</returns>
		public async Task<string> ScriptTagAsync(string style = "d")
		{
			var token = await _host.Token();

			return ScriptTag(token, style);
		}

		/// <summary>
		/// Get the API instance
		/// </summary>
		/// <returns></returns>
		internal Api Api()
		{
			return _host;
		}
	}
}
